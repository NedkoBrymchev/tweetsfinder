//
//  PostTableViewCell.swift
//  TweetsFinder
//
//  Created by Nedko S. Bramchev on 10.10.18.
//  Copyright © 2018 Nedko S. Bramchev. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userImage: ImageViewWithURLString!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var postImage: ImageViewWithURLString!
    @IBOutlet weak var postImageHeight: NSLayoutConstraint!
    @IBOutlet weak var postImageWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.userImage.clipsToBounds = true
        self.userImage.layer.cornerRadius = 25
    }
    
    func populateCellTwitterPost(post:TwitterPost){
        self.dateLabel.text = post.created_at
        self.descriptionLabel.text = post.text
        self.labelUsername.text = post.user?.name
        self.userImage.imageURLString = post.user?.profile_image_url_https
        
        //download and set postImage
        postImage.image = UIImage()
        if let media = post.entities?.media, media.count > 0{
            postImage.imageURLString = media[0].media_url_https!
            
            if let size = postImage.image?.size{
                if(size.width  > descriptionLabel.frame.width){
                    let k = descriptionLabel.frame.width / (postImage.image?.size.width ?? 1)
                    self.postImageWidth.constant = k * size.width
                    self.postImageHeight.constant = k * size.height
                }
                else{
                    self.postImageWidth.constant = size.width
                    self.postImageHeight.constant = size.height
                }
            }
            else{
                self.postImageWidth.constant = 100
                self.postImageHeight.constant = 100
            }
        }
        else{
            self.postImageWidth.constant = 0
            self.postImageHeight.constant = 0
        }
    }
    
    class func reuseIdentifier() -> String {
        return "PostTableViewCell"
    }
}
