//
//  TwitterMedia.swift
//  TweetsFinder
//
//  Created by Nedko S. Bramchev on 10.10.18.
//  Copyright © 2018 Nedko S. Bramchev. All rights reserved.
//

import Foundation

struct TwitterMedia:Codable {
    let media_url_https:String?
}
