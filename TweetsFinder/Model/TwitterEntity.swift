//
//  TwitterEntity.swift
//  TweetsFinder
//
//  Created by Nedko S. Bramchev on 10.10.18.
//  Copyright © 2018 Nedko S. Bramchev. All rights reserved.
//

import Foundation

class TwitterEntity:Codable {
    let media:[TwitterMedia]?
    
    private enum CodingKeys: String, CodingKey {
        case media
    }
    
    required init(from decoder: Decoder) throws {
        
        // Get our container for this subclass' coding keys
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let media = try container.decodeIfPresent([TwitterMedia].self, forKey: .media) {
            self.media = media
        }
        else{
            media = []
        }
    }
}
