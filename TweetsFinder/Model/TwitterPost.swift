//
//  TwitterPost.swift
//  TweetsFinder
//
//  Created by Nedko S. Bramchev on 10.10.18.
//  Copyright © 2018 Nedko S. Bramchev. All rights reserved.
//

import Foundation

class TwitterPost:Codable {
    let text:String?
    let created_at:String?
    var entities:TwitterEntity?
    var user:TwitterUser?
    
    private enum CodingKeys: String, CodingKey {
        case entities
        case text
        case created_at
        case user
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        text = try container.decode(String.self, forKey: .text)
        created_at = try container.decode(String.self, forKey: .created_at)
        if let entities = try container.decodeIfPresent(TwitterEntity.self, forKey: .entities) {
            self.entities = entities
        }
        else{
            self.entities = nil
        }
        if let user = try container.decodeIfPresent(TwitterUser.self, forKey: .user) {
            self.user = user
        }
        else{
            self.user = nil
        }
    }
}

