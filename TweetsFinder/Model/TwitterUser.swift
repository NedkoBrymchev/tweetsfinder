//
//  TwitterUser.swift
//  TweetsFinder
//
//  Created by Nedko S. Bramchev on 10.10.18.
//  Copyright © 2018 Nedko S. Bramchev. All rights reserved.
//

import Foundation

struct TwitterUser:Codable {
    let name:String?
    let profile_image_url_https:String?
}
