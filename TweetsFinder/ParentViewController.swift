//
//  ParentViewController.swift
//  TweetsFinder
//
//  Created by Nedko S. Bramchev on 10.10.18.
//  Copyright © 2018 Nedko S. Bramchev. All rights reserved.
//

import UIKit

class ParentViewController: UIViewController {
    
    private var loadingIndicator:UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(loadingIndicator)
        self.loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.loadingIndicator.tintColor = .black
        self.loadingIndicator.activityIndicatorViewStyle = .whiteLarge
        self.loadingIndicator.color = .black
        
        
        let xConstraint = NSLayoutConstraint(item: loadingIndicator, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
        let yConstraint = NSLayoutConstraint(item: loadingIndicator, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: 0)
        
        self.view.addConstraints([xConstraint,yConstraint])
        self.loadingIndicator.isHidden = true
    }
    
    func addLoadingIndicator(){
        self.view.bringSubview(toFront: self.loadingIndicator)
        self.loadingIndicator.isHidden = false
        self.loadingIndicator.startAnimating()
    }
    
    func removeLoadingIndicator(){
        self.loadingIndicator.isHidden = true
        self.loadingIndicator.stopAnimating()
    }
    
}


