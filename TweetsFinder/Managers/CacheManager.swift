//
//  CacheManager.swift
//  TweetsFinder
//
//  Created by Nedko S. Bramchev on 10.10.18.
//  Copyright © 2018 Nedko S. Bramchev. All rights reserved.
//

import UIKit

final class CacheManager {
    
    var cachedImages:[String:UIImage] = [:]
    
    static let sharedInstance = CacheManager()
    
}
