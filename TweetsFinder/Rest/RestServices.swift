//
//  RestServices.swift
//  TweetsFinder
//
//  Created by Nedko S. Bramchev on 10.10.18.
//  Copyright © 2018 Nedko S. Bramchev. All rights reserved.
//

import Foundation
import SwiftyJSON
import OAuthSwift

class RestServices {
    let baseURL = "https://api.twitter.com/1.1/search/tweets.json"
    let twitterSearchKey = "q"
    let twitterConsumerKey = "DGZVogdlxWk7RTQoUrxlrEDqj"
    let twitterConsumerSecret = "GLL7OkqfUjMrpj72Y8VsqqCs0icibTEYDF7blLrY7pM7d1JYlZ"
    
    static let sharedInstance = RestServices()
    
    func getTweetsThatContainsSearchedString(searchString: String, onSuccess: @escaping([TwitterPost]) -> Void, onFailure: @escaping(Error) -> Void){
        
        let oauthswift = OAuth1Swift(
            consumerKey:    twitterConsumerKey,
            consumerSecret: twitterConsumerSecret
        )
        
        _ = oauthswift.client.get(baseURL,
                                  parameters:[twitterSearchKey:searchString],
                                  success: { response in
                                    
                                    let jsonData = response.string?.data(using: .utf8)!
                                    let decoder = JSONDecoder()
                                    let posts = try? decoder.decode([TwitterPost].self, from: jsonData!,keyPath:"statuses")
                                    
                                    onSuccess(posts ?? [])
        },
                                  failure: { error in
                                    onFailure(error)
        })
        
    }
}
