//
//  TweetsFinderViewController.swift
//  TweetsFinder
//
//  Created by Nedko S. Bramchev on 10.10.18.
//  Copyright © 2018 Nedko S. Bramchev. All rights reserved.
//

import UIKit
import Foundation

class TweetsFinderViewController: ParentViewController, UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchButton: UIButton!
    
    var chatToSelect:String = ""
    
    private var posts:[TwitterPost] = []
    private var selectedPost:TwitterPost!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for s in searchBar.subviews[0].subviews {
            if let textfield = s as? UITextField {
                textfield.textColor = .white
                textfield.layer.borderWidth = 1.0
                textfield.layer.cornerRadius = 10
                textfield.layer.borderColor = UIColor.white.cgColor
                textfield.backgroundColor = .white
            }
        }
        
        self.tableView.register(UINib(nibName:PostTableViewCell.reuseIdentifier(),bundle: nil), forCellReuseIdentifier: PostTableViewCell.reuseIdentifier())
        
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    //MARK:  UITableViewDelegate, UITableViewDataSource
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return NO_HEADER_FOOTER_VIEW_HEIGHT
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return NO_HEADER_FOOTER_VIEW_HEIGHT
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PostTableViewCell.reuseIdentifier(), for: indexPath) as! PostTableViewCell
        cell.populateCellTwitterPost(post: self.posts[indexPath.row])
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.searchBar.resignFirstResponder()
    }
    
    //MARK: - Searchbar delegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if !(searchBar.text?.isEmpty ?? true){
            self.getTweets()
        }
    }
    
    //MARK: - Actions
    @IBAction func searchButtonClicked(_ sender: Any) {
        self.searchBarSearchButtonClicked(self.searchBar)
    }
    
    //MARK: - Private methods
    private func getTweets(){
        if(self.searchBar.text?.isEmpty ?? true){
            //present alert view for empty text
        }
        else{
            //get post for searched text
            self.addLoadingIndicator()
            RestServices.sharedInstance.getTweetsThatContainsSearchedString(searchString: searchBar.text ?? "", onSuccess: {[weak self] posts in
                self?.posts = posts
                self?.tableView.reloadData()
                self?.removeLoadingIndicator()
                }, onFailure: { [weak self] error in
                    self?.removeLoadingIndicator()
                    //present alert with error
            })
        }
    }
}
